@if(!empty($requestTypes))
    <div class="card br-smooth">
        <div class="card-header br br-top-smooth">
            {{__('Создание запроса')}}
        </div>
        <div class="card-body">
            <div id="user-request" class="bg-white br-smooth br-sm-secondary clearfix mg-5 pg-5">
                <a onclick="postRequestForm()" class="btn mg-5  br-smooth-l border-0 btn-outline-secondary float-left"><i class="fa fa-search"></i></a>
                <a onclick="postRequestForm()" class="btn mg-5 br-smooth-r border-0 btn-outline-secondary float-right"><i class="fa fa-search"></i></a>
                <input id="request-name" type="text" class="br-smooth mg-5 custom-form-control float-left" placeholder="Название запроса" aria-label="Название запроса" aria-describedby="basic-addon1" style="height: 2rem ;width: 15%">

            </div>

            <div id="c">
                <div class="form-group row pg-l-r-15p" style="width: 90%" >
                    <a onclick="getPriceWindow()" class="btn btn-outline-secondary br-smooth">{{__('Цена')}} <i class="fa fa-plus mg-0-5"></i></a>
                @foreach($requestTypes as $requestType)

                    <a onclick="loadParametersForType({{$requestType->id }}, {{'"'.$requestType->start_of_request.'"'}})" class="btn btn-outline-secondary br-smooth">{{__($requestType->name)}} <i class="fa fa-plus mg-0-5"></i></a>
                @endforeach
                </div>
            </div>
        </div>
    </div>
@endif
