<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class GlobalRequest extends Model
{
    protected $fillable = [
        'request',
        'value',
        'last_updated_size',
    ];
    public function userRequests()
    {
        return $this->belongsToMany('App\UserRequests','user_request_global_requests','global_request','user_request');
    }
    public function tenders()
    {
        return $this->belongsToMany('App\Tenders','global_request_tenders','global_request_id','tender_id');
    }
    public function tags()
    {
        return $this->belongsToMany('App\Settings','global_request_settings');
    }
}
