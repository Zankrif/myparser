function dropTags() {
    document.getElementById("dropdownTags").classList.toggle("show");
}


function filterFunction()
{
    let inputString = document.getElementById("inputValue")
        .value.toUpperCase();
    let nodeArray = document.getElementById("dropdownTags")
        .getElementsByTagName("a");
    for(var i = 0 ; i < nodeArray.length ; i++) {
        let TextValue = nodeArray[i].textContent || nodeArray[i].innerText;
        nodeArray[i].style.display =  (TextValue.toUpperCase().indexOf(inputString) > -1)? "":"none";
    }

}

