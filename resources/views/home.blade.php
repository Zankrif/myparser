@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header">{{ __('Dashboard') }}</div>

                <div class="card-body">
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif
                        <div class="row justify-content-center" style="margin:auto ;">
                            <a class="btn btn-primary" href="{{ route('request.get') }}"> Создать запрос </a>
                        </div>

                        <div class="targets">
                            <h5> Список ваших запросов </h5>
                            <table class="table">
                                <thead class="thead-dark">
                                <tr>
                                    <th>
                                        Имя запроса
                                    </th>
                                    <th>
                                        Количество элементов
                                    </th>
                                    <th>

                                    </th>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach($requests as $request)
                                    <tr>
                                       <td>
                                           {{$request->name}}
                                       </td>
                                        <td>
                                            {{$request->requests->last_updated_size}}
                                        </td>
                                        <td>
                                            <a class="btn btn-primary" href="{{ route('request.tenders.get',['request'=>$request->requests]) }}">Тендеры</a>
                                        </td>
                                    </tr>

                                @endforeach
                                </tbody>
                                {!! $links !!}
                            </table>
                        </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
