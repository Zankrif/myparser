<div  class="card pg-15 parameters-list br-smooth shadow-lg ">
    <meta name="csrf-token" content="{{ csrf_token() }}" />
    <div id="point" hidden>{{$point}}</div>
    <div class="row justify-content-end">
        <a onclick="closeParametersList()" class="btn btn-secondary  " style="width: max-content; margin-right: 2rem"><span class="fa fa-close"></span></a>
    </div>
    <div id="dropdownTags" class="dropdown-content form-group list-group border-0 mg-5" >

            <input id="inputValue" onkeyup="searchFunction()" type="text" class="form-control  " placeholder="tag..." style="width: 95%; margin: 0 10px"  aria-describedby="basic-addon1">


        <div style="padding: 5px; max-height: 200px;  overflow:auto;">
            @foreach($parameters as $parameter)
                <a onclick="attachNewParameter({{$parameter->id}},{{'"'.$parameter->user_value.'"'}},{{'"'.$parameter->request_value.'"'}})" class="btn btn-outline-secondary list-inline-item mg-5"  style="width: 95%;">{{$parameter->user_value}}</a>
            @endforeach
        </div>
    </div>
</div>
