<?php


namespace App\Http\Controllers;


use App\Notifications\InvoicePaid;
use App\Notifications\TendersChanged;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Notification;

class NotificationController
{
    public function send($id , $massage)
    {
        $email = User::find($id)->email;

        Notification::route('mail', $email)
            ->route('nexmo', '5555555555')
            ->route('slack', 'https://hooks.slack.com/services/...')
            ->notify(new TendersChanged($massage));

    }
}
