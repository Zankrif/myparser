<?php

namespace App\Http\Repositories;
use App\UserRequestGlobalRequest;

class UserRequestGlobalRequestRepository
{
    /**
     * @param int $userRequestId
     * @param int $globalRequestId
     * @return UserRequestGlobalRequest
     */
    public function create($userRequestId, $globalRequestId)
    {
        return UserRequestGlobalRequest::firstOrCreate([
            'user_request_id'=>$userRequestId,
            'global_request_id'=>$globalRequestId,
        ]);
    }
}
