<div id="tenders" class="tenders">

    <h4> Список тендеров </h4>
    <table class="table ">
        <thead class="thead-dark" >
        <tr>
            <th></th>
            <th>
                ID
            </th>
            <th>
                Заголовок
            </th>
            <th>
                Цена
            </th>
            <th>
                Дата
            </th>
            <th>

            </th>
        </tr>
        </thead>
        <tbody>

        @foreach($tenders as $tender)

            <tr>
                <td><a class="btn btn-warning" href="#" onclick="getTender({{$tender}})">Детально</a></td>
                <td>{{ $tender->tender }}</td>
                <td>{{mb_strimwidth( $tender->title,0,50,"....") }}</td>
                <td>{{ $tender->value }}</td>
                <td>{{ $tender->date }}</td>
                <td><a class="btn btn-warning" href="#" onclick="getTender({{$tender}})">Детально</a></td>
            </tr>

        @endforeach
        </tbody>
        {!! $tenders->links()  !!}
    </table>
    {!! $tenders->links()  !!}
    <div id="requestId" hidden>
        {{$requestId}}
    </div>

</div>
