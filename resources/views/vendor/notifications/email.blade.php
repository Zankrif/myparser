
@component('mail::layout')
{{-- Header --}}
@slot('header')
@component('mail::header', ['url' => config('app.url')])
{{ config('app.name') }}
<link href="{{ asset('css/app.css') }}" rel="stylesheet">
@endcomponent
@endslot

{{-- Body --}}
В вашем запросе произошли изменения.
@component('mail::table')
| Тендер        | Ссылка        |
| ------------- |:-------------:|
@foreach($tenders as $tender)
|{{$tender['tender']}}|<a href="{{$tender['link']}}" style=" background-color: #008CBA;border: none; border-radius: 2px; color: white;padding: 15px 32px;text-align: center;text-decoration: none;display: inline-block;font-size: 16px;">Перейти</a>|
@endforeach
@endcomponent

{{-- Subcopy --}}
@isset($subcopy)
@slot('subcopy')
@component('mail::subcopy')
{{ $subcopy }}
@endcomponent
@endslot
@endisset

{{-- Footer --}}
@slot('footer')
@component('mail::footer')
&copy; {{ date('Y') }} {{ config('app.name') }}. All rights reserved.
@endcomponent
@endslot
@endcomponent
