<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class TendersChangedMail extends Mailable
{
    use Queueable, SerializesModels;

    protected $tenders;

    /**
     * Create a new message instance.
     * @param array $tenders
     * @return void
     */
    public function __construct( $tenders)
    {
        $this->tenders = $tenders;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {

        $this->view('/notification/tendersChangedMail')->with(['tenders'=>$this->tenders]);

        return   $this->markdown('/vendor/notifications/email');

    }
}
