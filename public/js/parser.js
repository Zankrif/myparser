
$(document).ready(function () {
    //Get paginated page of created by user requests
    $(document).on('click', '#requests .pagination a', function (event) {
        event.preventDefault();
        var page = $(this).attr('href').split('page=')[1];
        fetch_data("/user/requests/", page);
    });
    //Get paginated tenders for some selected Request
    $(document).on('click', '#tenders .pagination a', function (event) {
        event.preventDefault();
        var page = $(this).attr('href').split('page=')[1];
        fetch_data("/user/request/tenders", page);
    });


    //Function for pagination

    function fetch_data(url, page) {
        var data = $("#requestId").text();
        $.ajax({
            url: url,
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            data: {data, page},
            success: function (data) {
                clearSpace('parameters');
                clearSpace('user-space');
                $('#user-space').wrapInner(data);
            }
        });
    }
})

function getRequests(){
    clearSpace('user-space');
    ajaxRequest('/user/requests/','GET',null,'user-space');
}


function closeTender(){
    clearSpace('tender');
}

function getTenders(requestId){
    clearSpace('user-space');
    ajaxRequest('/user/request/tenders','GET',{data:requestId},'user-space');
}
function getTender($tender) {
    clearSpace('tender');
    ajaxRequest('/user/request/tender/','GET',{tender:$tender},'tender');
}
function getRequestForm() {
    clearSpace('user-space');
    ajaxRequest('/user/request/form','GET',null, 'user-space');
}
function ajaxRequest(url, type, data, folder) {
    $.ajax({
        type:type,
        url:url,
        header: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
        data:data,

        success: function (data){
            clearSpace(folder);
            $('#'+folder).wrapInner(data);
        },
        error: function(data) {
            var errors = data.responseJSON;
            var errorsHtml = '';
            $.each(errors.errors, function( key, value ) {
                errorsHtml += '<p class="text-danger">' + value[0] + '</p>';
            });
            $("#"+folder).wrapInner(errorsHtml);
        },
    })
}
function clearSpace(folder) {
    $('#'+folder).empty();
}
function removeFromSpace(folder){
    $('#'+folder).remove();
}


///USER REQUESTS
let userRequestParametersArray = {};
let userRequestValuesArray = [];
//ARRAY FUNCTIONS
function addParameterToArray($requestParameterId, $typeStartPoint, $requestParameterValue) {
    let request = $requestParameterId+','+$typeStartPoint+','+$requestParameterValue;
    userRequestParametersArray[$requestParameterId] = $requestParameterId+','+$typeStartPoint+','+$requestParameterValue ;
}

function deleteParameterFromArray($requestParameterId) {
    delete userRequestParametersArray[$requestParameterId];
}

//UI functions

function loadParametersForType($typeId, $typeStartPoint) {
    var data = {
        id: $typeId,
        point: $typeStartPoint,
    };
    ajaxRequest('/user/request/form/type/parameters/','GET', data, 'parameters');
}



function attachNewParameter($parameterId,$userValue,$requestValue) {
    if(!$('*').is('#'+$parameterId))
    {
        $('#user-request').append($('<div/>', {
            id: $parameterId,
            "class":'btn  border-0 br-smooth mg-5  btn-secondary float-left',
            "style":"height:100%;",
            text: $userValue,
            "onclick":'detachParameter('+$parameterId+')',

        }));
        $('#'+$parameterId).append($('<span/>',{"class":"fa fa-close mg-l-10"}));
        var point =  $('#point').text();
        addParameterToArray($parameterId, point, $requestValue)
    }else{
        alert('Вы уже добавили этот парметер');
    }
}

function detachParameter($parameterId) {
    deleteParameterFromArray($parameterId)
    removeFromSpace($parameterId);
}
function closeParametersList() {
    clearSpace('parameters');
}

function searchFunction() {
    let inputString = document.getElementById("inputValue")
        .value.toUpperCase();
    let nodeArray = document.getElementById("dropdownTags")
        .getElementsByTagName("a");
    for(var i = 0 ; i < nodeArray.length ; i++) {
        let TextValue = nodeArray[i].textContent || nodeArray[i].innerText;
        nodeArray[i].style.display =  (TextValue.toUpperCase().indexOf(inputString) > -1)? "":"none";
    }
}
function postRequestForm() {

    getValuesFromForm();
    if (!userRequestValuesArray) {
        userRequestValuesArray = null;
    }
    if ($.isEmptyObject(userRequestParametersArray)) {
        return alert('Укажите параметры запроса');
    }

    let name = $("#request-name").val();
    if (!name) {
        return alert('Укажите название запроса');
    }
    $.ajax({
        url: '/user/request/form/post',
        type: "POST",
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        },
        data: {
            'parameters': userRequestParametersArray || null,
            'name': name,
            'value': userRequestValuesArray,
        }, success: function () {
            getRequests();
        },
        error: function(data) {
            var errors = data.responseJSON;
            var errorsHtml = '';
            $.each(errors.errors, function( key, value ) {
                errorsHtml += '<p class="text-danger">' + value[0] + '</p>';
            });
            clearSpace('user-space .header');
            $("#user-space .header").wrapInner(errorsHtml);
        },
    })
}
function getValuesFromForm() {
    userRequestValuesArray = [];
    userRequestValuesArray.push($('#request-min-price').val().replace(/ /g, '') ,$('#request-max-price').val().replace(/ /g, '')) ;
}
function closeValue(){
    $('#value').remove();
    userRequestValuesArray = null;
}
function getPriceWindow() {
    if(!$('*').is('#value')) {
        $('#user-request').append(
            '<div id="value"  class="mg-5 custom-form-folder float-left" style="padding: 0.2rem;">' +
            '<input id="request-min-price" type="text" class="br-smooth custom-form-control  mg-l-r-5 float-left" placeholder="Мин цена" aria-label="Мин цена" aria-describedby="basic-addon1" style="width: 40%">' +
            '<input id="request-max-price" type="text" class="br-smooth custom-form-control mg-l-r-5 float-left" placeholder="Макc цена" aria-label="Макc цена" aria-describedby="basic-addon1" style="width: 40%">' +
            '<span onclick="closeValue()" class="btn btn-secondary  fa fa-close" style="height: 100% ;width: 15%"></span>' +
            '</div>'
        );
    }else{
        alert('Вы уже добвили поле цены');
    }

}

