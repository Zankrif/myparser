<?php

namespace App\Console\Commands;

use App\GlobalRequest;
use App\Http\Services\ParserService;
use App\Targets;
use Illuminate\Console\Command;

class parse extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'parse';
    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'parse all requests';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     */
    public function handle()
    {

        $this->info('Start Parser','v');
        $requestModels = GlobalRequest::all();// Get all users requests

        $bar = $this->output->createProgressBar(count($requestModels));
        $bar->start();
        foreach ($requestModels as $requestModel) {


            $this->info(PHP_EOL.'Inspecting Request ID: '.$requestModel->id.PHP_EOL,'v');
            $messages =  app(ParserService::class)->parse($requestModel);
            $this->warn($messages['errors'].PHP_EOL,'v');
            $this->info($messages['targets'], 'v');
            $this->line('Already have :'.PHP_EOL.$messages['Already have'],null,'vvv');
            $this->info('Added :'.PHP_EOL.$messages['Added'],'vv');
            $this->warn('Unsubscribed :'.$messages['Unsubscribed'], 'vv');
            $bar->advance();
        }
        $this->info(PHP_EOL.'Parser end work'.PHP_EOL,'v');
        $bar->finish();
    }
}
