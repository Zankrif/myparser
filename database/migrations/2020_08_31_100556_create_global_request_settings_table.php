<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateGlobalRequestSettingsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('global_request_settings', function (Blueprint $table) {
            $table->id()->unique();
            $table->timestamps();
        });

        Schema::table('global_request_settings', function (Blueprint $table){
            $table->unsignedBigInteger('global_request_id');
            $table->foreign('global_request_id')->references('id')->on('global_requests');
            $table->unsignedBigInteger('settings_id');
            $table->foreign('settings_id')->references('id')->on('settings');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('global_request_settings');
    }
}
