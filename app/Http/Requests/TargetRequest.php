<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class TargetRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }
    public function messages()
    {
        return [
            'name.*'=>'Название запроса должно быть не менее 3ех символов',
            'parameters.required' => 'Укажите как минимум один параметр для поиска (Указание цены не входит в этот перечень)',
            'value.*.*' => 'Цена должна быть указан как число ',
        ];
    }
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name'=>'required|string|min:3',
            'value.*' => 'nullable|integer|min:0',
            'parameters'=>'required|array',
        ];
    }
}
