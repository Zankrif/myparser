<?php


namespace App\Http\Controllers;

use App\GlobalRequest;
use App\Http\Requests\TargetRequest;
use App\Http\Services\RequestService;
use App\RequestType;
use App\UserRequest;

use Illuminate\Http\Request;

class AjaxController extends Controller
{

    /**
     * Method returns main view
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index()
    {
        return view('parser');
    }

    /**
     * Method expect Ajax Request witch contain User model
     * Model used to return requests witch was created by User
     *
     * If request is not a Ajax type. Method return abort 403
     * @param Request $request
     * @return string
     * @throws \Throwable
     */
    public function requests(Request $request)
    {
        if ($request->ajax()) {
            $paginate = UserRequest::where('user_id', $request->user()->id)->paginate(5);
            $links = $paginate->links();
            $requests = $paginate->each(function ($node) {
                return $node['requests'] = $node->request()->first();
            });
            return view('layouts/userRequests', ['requests' => $requests, 'links' => $links])->render();
        }
        return abort(403);
    }

    /**
     * Method expect Ajax Request
     * Method return rendered view witch contain
     * A list of paginated Tenders for USER selected Request
     *
     * @param Request $request
     * @return string
     * @throws \Throwable
     */
    public function tenders(Request $request)
    {
        if ($request->ajax()) {

            $requestId = $request->input('data');
            /** @var RequestService $requestService */
            $requestService = app(RequestService::class);
            $requestModel = $requestService->getRequestById($requestId);

            $tenders = $requestModel->tenders()->paginate(7);
            return view('layouts/tenders', compact('tenders', 'requestId'))->render();
        }
        return abort(403);
    }

    /**
     * Method weight for model Tender
     * witch will be rendered and return
     * @param Request $request
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\View\View|void
     */
    public function tender(Request $request)
    {
        if ($request->ajax()) {
            $tender = $request->input('tender');
            return view('tenderAjax', compact('tender'));
        }
        return abort(403);
    }

    /**
     * Method expect Ajax Get request
     * Than return rendered view of Request Form
     * @param Request $request
     * @return array|string|void
     * @throws \Throwable
     */
    public function requestFormGet(Request $request)
    {
        if ($request->ajax()) {
            /** @var RequestService $requestService */
            $requestService = app(RequestService::class);
            $requestTypes = $requestService->getRequestsTypes();
            return view('requestForm', compact('requestTypes'))->render();
        }
        return abort('403', 'К сожалению ваш запрос отклонен по причине его не коректности');
    }

    /**
     * Method return rendered view of Parameters List
     * @param Request $request
     * @return string
     * @throws \Throwable
     */
    public function parameters(Request $request)
    {

        if ($request->ajax()) {
            /** @var RequestService $requestService */
            $requestService = app(RequestService::class);
            $parameters = $requestService->getParameters($request->input('id'));
            $point = $request->input('point');
            return view('layouts/userRequestParameters', compact('parameters', 'point'))->render();
        }
        return abort('403', 'К сожалению ваш запрос отклонен по причине его не коректности');
    }

    /**
     * Method expect a validated data from form
     * to send them into Request Service
     * @param TargetRequest $request
     */
    public function postRequest(TargetRequest $request)
    {
        if ($request->ajax()) {
            $requestService = app(RequestService::class);;
            $requestService->add(
                $request->user()->id,
                $request->input('name'),
                $request->input('value'),
                $request->input('parameters'));
        }
    }
}
