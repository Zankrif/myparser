<?php


namespace App\Http\Repositories;


use App\RequestType;

class RequestTypeRepository
{
    /**
     * @return RequestType[]|\Illuminate\Database\Eloquent\Collection
     */
    public function getAll()
    {
        return RequestType::all();
    }

    /**
     * @param int $id
     * @return RequestType|null
     */
    public function findById($id): ?RequestType
    {
        return RequestType::find($id);
    }

    /**
     * @param int $id
     * @return \Illuminate\Database\Eloquent\Collection|null
     */
    public function parameters($id)
    {
        $typeModel = $this->findById($id);
        if(!empty($typeModel)) {
            return $typeModel->settings()->get();
        }
        return null;
    }

    /**
     * @param string $nameForUser
     * @param string $startOfRequest
     * @return RequestType
     */
    public function add($nameForUser, $startOfRequest)
    {
        return RequestType::firstOrCreate([
            'name' => $nameForUser,
            'start_of_request' => $startOfRequest,
        ]);
    }

    /**
     * @param RequestType $model
     * @param string $nameForUser
     * @param string $startOfRequest
     * @return bool|null
     */
    public function update($model, $nameForUser, $startOfRequest)
    {
        return $model->update([
            'name' => $nameForUser,
            'start_of_request' => $startOfRequest,
        ]);
    }

    /**
     * @param RequestType $model
     * @return bool|null
     * @throws \Exception
     */
    public function delete($model)
    {
        return $model->delete();
    }
}
