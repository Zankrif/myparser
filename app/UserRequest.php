<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class UserRequest extends Model
{
    protected $fillable = [
        'user_id',
        'name',
    ];
    public function user()
    {
        return $this->belongsTo('App\Users','user_id');
    }
    public function request()
    {
        return $this->belongsToMany('App\GlobalRequest','user_request_global_requests','user_request_id','global_request_id');
    }

}
