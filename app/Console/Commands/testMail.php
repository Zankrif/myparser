<?php

namespace App\Console\Commands;

use App\Notifications\TendersChanged;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\Notification;

class testMail extends Command
{
    CONST tenders = [
        [
            'tender' => 'ID BLABLABLA',
            'link'=>'https://google.com',
        ]
    ];

    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'test:mail';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        Notification::route('mail', 'zpedets.ua@gmail.com')
            ->route('nexmo', '5555555555')
            ->route('slack', 'https://hooks.slack.com/services/...')
            ->notify(new TendersChanged(self::tenders));
    }
}
