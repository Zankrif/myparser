<?php


namespace App\Http\Repositories;

use App\Settings;

class SettingsRepository
{
    /**
     * @param int $requestType
     * @param string $requestValue
     * @param string $userValue
     * @return Settings
     */
    public function add($requestType, $requestValue, $userValue)
    {
        return Settings::firstOrCreate([
            'type' => $requestType,
            'request_value' => $requestValue,
            'user_value' => $userValue,
        ]);
    }

    /**
     * @param Settings $model
     * @param string $requestValue
     * @param string $userValue
     * @return bool
     */
    public function update($model, $requestValue, $userValue)
    {
        return $model->update([
            'request_value' => $requestValue,
            'user_value' => $userValue,
        ]);
    }

    /**
     * @return Settings[]|\Illuminate\Database\Eloquent\Collection
     */
    public function getAll()
    {
        return Settings::all();
    }

    /**
     * @param Settings $model
     * @return mixed
     * @throws \Exception
     */
    public function delete($model)
    {
        return $model->delete();
    }
}
