<?php
namespace App\Http\Repositories;
use App\GlobalRequestSettings;

class GlobalRequestSettingsRepository
{
    /**
     * @param int $globalRequestId
     * @param int $settingId
     * @return GlobalRequestSettings
     */
    public function add($globalRequestId, $settingId)
    {
        return GlobalRequestSettings::firstOrCreate([
            'global_request_id'=>$globalRequestId,
            'setting_id'=>$settingId,
        ]);
    }

    /**
     * @param array $array
     * @return bool|null
     */
    public function insert ($array)
    {
        return GlobalRequestSettings::insert($array);
    }

    /**
     * @param int $globalRequestId
     * @return GlobalRequestSettings|null
     */
    public function first($globalRequestId)
    {
        return GlobalRequestSettings::where('global_request_id',$globalRequestId)->first();
    }

    /**
     * @param int $globalRequestId
     * @return bool
     * @throws \Exception
     */
    public function deleteRequest($globalRequestId)
    {
        return GlobalRequestSettings::where('global_request_id',$globalRequestId)->delete();
    }

    /**
     * @param int $settingId
     * @return bool
     * @throws \Exception
     */
    public function deleteWithTag($settingId)
    {
        return GlobalRequestSettings::where('setting_id',$settingId)->delete();
    }
}
