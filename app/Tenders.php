<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Tenders extends Model
{

    protected $fillable = [
        'tender',
        'title',
        'description',
        'company',
        'value',
        'status',
        'region',
        'date',
    ];

}
