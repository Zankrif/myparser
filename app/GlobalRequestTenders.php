<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class GlobalRequestTenders extends Model
{
    protected $fillable = [
        'global_request_id',
        'tender_id',
    ];
}
