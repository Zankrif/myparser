
    <div class="card">
        <div class="card-header ">
            <h3>{{__('Тендер:  '.$tender['tender'])}}</h3>

        </div>
        <div class="card-body">
            <div style="text-align: right">
                <h3 style= "color: lightgreen">
                    {{__($tender['value'])}}

                </h3>
                <h4 style="color: cornflowerblue">
                    {{__("Статус :   ".$tender['status'])}}
                </h4>
                <h4 >
                    {{__("Регион :   ".$tender['region'])}}
                </h4>
                <h4 class="font-weight-lighter" style="font-size: 20px">
                    {{__($tender['company'])}}
                </h4>
            </div>
            @if(!empty($tender['title']))
                <h5 style="margin: 30px 0">
                    {{__(''.$tender['title']) }}
                </h5>
            @endif
            @if(!empty($tender['description']))
                <p  style="font-size: 15px; margin: 30px 0">
                    {{__($tender['description'])}}
                </p>
            @endif
            <p style="font-size: 15px; text-align: right">
                {{__($tender['date'])}}
            </p>

            <a class="btn btn-outline-secondary" href="{{'https://prozorro.gov.ua/tender/'.substr($tender['tender'],4)}}">Перейти на сайт</a>
        </div>
        <a onclick="closeTender()" class="btn btn-lg btn-outline-secondary" >X</a>
    </div>

