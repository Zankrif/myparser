<?php

namespace App\Console\Commands;

use App\RequestType;
use Illuminate\Console\Command;

class tagsTypeShow extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'tagsType:show';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     */
    public function handle()
    {
        $models =  RequestType::select('id','start_of_request')->get();
        foreach ($models as $model)
        {
            $this->info('ID : '.$model->id);
            $this->info('REQUEST = '. $model->start_of_request);
        }


    }
}
