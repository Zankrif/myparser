<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Settings extends Model
{
    protected $fillable = [
        'type',
        'request_value',
        'user_value',
    ];

    public function requests()
    {
        return $this->belongsToMany('App\Targets', 'request_descriptions', 'request_tags', 'request_id');
    }

    public function type()
    {
        return $this->belongsTo('App\RequestType', 'type')->get()->start_of_request;
    }
}
