<?php


namespace App\Http\Repositories;


use App\GlobalRequest;

class GlobalRequestRepository
{

    /**
     * @param int $id Primary key for Global Request Table
     * @return GlobalRequest|null
     */
    public function find($id): ?GlobalRequest
    {
        return GlobalRequest::find($id);
    }

    public function first($request,$value)
    {
        return GlobalRequest::where([
            ['request','=',$request],
            ['value','=',$value]
        ])->first();
    }

    /**
     * @param string $request for example region=69&value=2000&
     * @param  string|null $value .. example 1000-2000 or 1000  or null
     * @return int
     */
    public function firsOrCreate(string $request,  $value)
    {
        $requestModel = $this->first($request,$value);
        if(empty($value))
        {
            $value = '';
        }
        if(empty($requestModel)){
            return GlobalRequest::create([
                'request'=> $request,
                'value'=>$value,
                'last_update_size'=>0,
            ])->id;
        }else{
            return $requestModel->id;
        }
    }

    /**
     * @param GlobalRequest $model
     * @param int $size
     * @return bool
     */
    public function update(GlobalRequest $model, $size)
    {
        return $model->update([
            'last_updated_size' => $size
        ]);
    }

    /**
     * @param GlobalRequest $model
     * @return bool|null
     * @throws \Exception
     */
    public function delete(GlobalRequest $model)
    {
        return $model->delete();
    }

}
