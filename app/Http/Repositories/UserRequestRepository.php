<?php


namespace App\Http\Repositories;


use App\UserRequest;

class UserRequestRepository
{
    /**
     * @param int $userId
     * @return UserRequest[]|null
     */
    public function userRequests($userId)
    {
        return UserRequest::select('user_id','name')::where('user_id',$userId)->get();
    }

    /**
     * @param int $id
     * @param string $name
     * @return UserRequest
     */
    public function add($id,$name)
    {
        return UserRequest::firstOrCreate([
            'user_id'=>$id,
            'name'=>$name,
        ]);
    }

    /**
     * @param int $id
     * @return UserRequest|null
     */
    public function find($id): ?UserRequest
    {
        return UserRequest::find($id);
    }

    /**
     * @param int $id
     * @return bool|null
     * @throws \Exception
     */
    public function deleteById($id): ?bool
    {
        return $this->find($id)->delete();
    }

    /**
     * @param UserRequest $model
     * @return bool|null
     * @throws \Exception
     */
    public function deleteByModel($model): ?bool
    {
        return $model->delete();
    }
}
