<?php

namespace App\Notifications;

use App\Mail\TendersChangedMail;
use Illuminate\Bus\Queueable;

use Illuminate\Notifications\Messages\MailMessage;
use Illuminate\Notifications\Notification;
use Illuminate\Support\Facades\Mail;

class TendersChanged extends Notification
{
    use Queueable;

    protected $tenders;
    /**
     * Create a new notification instance.

     * @param array $tenders
     * @return void
     */
    public function __construct($tenders)
    {
        $this->tenders = $tenders;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['mail'];
    }

    /**
     * @param $notifiable
     * @return TendersChangedMail
     */
    public function toMail($notifiable)
    {

        return (new TendersChangedMail($this->tenders))->to($notifiable->routes['mail']);
       /* return (new MailMessage)
            ->greeting('По вашему запросу случились изменения')
            ->line( $this->tenders)
            ->line('Спасибо за использование  нашего приложения')->;*/
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
            //
        ];
    }
}
