<div id="requests">
    <table class="table">
        @if(!empty($message))
            <div style="text-align: center">
                <label class="btn btn-danger" >{{ $message }}</label>
            </div>
        @endif
        <thead class="thead-dark br-smooth">
        <tr>
            <th scope="col">{{__('Название')}}</th>
            <th scope="col">{{__('Количество тендеров')}}</th>
            <th></th>
        </tr>
        </thead>
        <tbody>

        @foreach($requests as $request)
            <tr>
                <td>{{$request->name}}</td>
                @if($request->requests->last_updated_size == 0)
                    <td>{{__('Пожалуйста подождите пока информация о вашем запросе обновиться')}}</td>
                @else
                    <td>{{$request->requests->last_updated_size}} </td>
                @endif

                <td> <a onclick="getTenders({{$request['requests']->id}})" class="btn btn-outline-primary" >Тендеры</a></td>
            </tr>
        @endforeach
        <div>
            {!! $links !!}
        </div>
        </tbody>
    </table>
</div>

