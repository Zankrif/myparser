<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class RequestType extends Model
{
    protected $fillable = [
        'name',
        'start_of_request',
    ];
    public function settings()
    {
        return $this->hasMany('App\Settings','type');
    }
}
