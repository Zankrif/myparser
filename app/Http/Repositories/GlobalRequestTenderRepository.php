<?php


namespace App\Http\Repositories;


use App\GlobalRequestTenders;

class GlobalRequestTenderRepository
{
    /**
     * @param int $requestId
     * @param int $tenderId
     * @return GlobalRequestTenders
     */
    public function add($requestId, $tenderId)
    {
        return GlobalRequestTenders::create([
            'global_request_id' => $requestId,
            'tender_id' => $tenderId,
        ]);
    }

    /**
     * @param array $array
     * @return bool
     */
    public function insert($array)
    {
        return GlobalRequestTenders::insert($array);
    }

    /**
     * @param int $requestId
     * @param int $tenderId
     * @return bool|null
     * @throws \Exception
     */
    public function delete($requestId, $tenderId)
    {
        return GlobalRequestTenders::where([
            ['global_request_id','=',$requestId],
            ['tender_id','=',$tenderId],
        ])->delete();
    }

    /**
     * @param $requestId
     * @return bool|null
     * @throws \Exception
     */
    public function deleteWithTarget($requestId)
    {
        return GlobalRequestTenders::where('global_request_id',$requestId)->first()->delete();
    }
}
