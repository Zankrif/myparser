<div id="request" class="card">
    <div class="card-header">{{ __('Target Settings') }}</div>

    @foreach ($errors->all() as $error)
        <p class="btn btn-danger">{{ $error }}</p>
    @endforeach
    <div class="card-body">

        @if(!empty($message))
            <div style="text-align: center">
                <label class="btn btn-danger" >{{ $message }}</label>
            </div>
        @endif
        <form id="request-form">
            <div class="form-group row">
                <label for="name" class="col-md-4 col-form-label text-md-right">{{ __('Название Запроса') }}</label>


                <div class="col-md-6">
                    <input id="name" type="text" class="form-control  "  name="name"  required>
                </div>

            </div>

            <div class="form-group row">
                <label for="url" class="col-md-4 col-form-label text-md-right">{{ __('Url Сайта') }}</label>

                <div class="col-md-6">
                    <input id="url" type="text" class="form-control"  name="url" value="https://prozorro.gov.ua/tender/"  readonly >
                </div>
            </div>
            <div class="form-group row" style="padding: 0 25%;">

                <div class="input-group-prepend"   >
                    <button class="btn btn-outline-secondary dropdown-toggle" type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Цена</button>
                    <div class="dropdown-menu" style=" padding: 10px">
                        <div class="form-group row">
                            <div class="col-sm-10">
                                <label for="min-price">Минимальная цена</label>
                                <input id="min-price" type="text" class="form-control" name="value"   >
                            </div>
                        </div>
                        <div class="form-group row">
                            <div class="col-sm-10">
                                <label class="col-form-label" for="max-price">Максимальная цена</label>
                                <input id="max-price" type="text" class="form-control" name="value"  >
                            </div>
                        </div>
                    </div>
                </div>

                @foreach($requestParameters as $requestParameter)
                    <div class="input-group-prepend">
                        <button class="btn btn-outline-secondary dropdown-toggle" type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                            {{__($requestParameter['name'])}}
                        </button>
                        <div class="dropdown-menu" style="max-height:250px; overflow:auto; padding: 10px" >
                            @foreach($requestParameter['parameters'] as $parameter)
                                <div  class="form-check ">
                                    <input  id="{{$parameter->id}}" class="form-check-input"  type="checkbox" name="{{'parameters'}}" value="{{ $parameter->id.','.$requestParameter['startPoint'].','.$parameter->request_value}}">
                                    <label  class="form-check-label"  for="active.enquiries">{{$parameter->user_value}}</label>
                                </div>
                            @endforeach
                        </div>
                    </div>
                @endforeach
            </div>
            <div class="form-group row mb-0">
                <div class="col-md-6 offset-md-4">
                    <button  type="submit" class="btn btn-primary">
                        {{ __('Добавить') }}
                    </button>
                </div>
            </div>
        </form>
    </div>
</div>
