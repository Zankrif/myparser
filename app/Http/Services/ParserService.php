<?php


namespace App\Http\Services;

use App\Http\Repositories\GlobalRequestTenderRepository;
use App\Http\Repositories\TendersRepository;

use App\GlobalRequest;
use App\Http\Repositories\GlobalRequestRepository;
use App\Notifications\TendersChanged;

use App\User;
use Exception;
use GuzzleHttp\Client;
use GuzzleHttp\Exception\RequestException;
use GuzzleHttp\Psr7;

use Illuminate\Support\Facades\Notification;
use Symfony\Component\DomCrawler\Crawler;

class ParserService
{

    private const GUZZEL_PROPERTIES = [
        'base_uri' => 'https://prozorro.gov.ua/tender/', //Set url for Client
        'headers' => [
            'User-Agent' => 'Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:79.0) Gecko/20100101 Firefox/79.0',
            'X-Requested-With' => 'XMLHttpRequest',
            'Cache-Control' => 'max-age=0',
            'Origin' => 'https://prozorro.gov.ua',
            'Referer' => ' https://prozorro.gov.ua/',
        ]
    ];

    //Path's to inspected values used in crawler
    private const SIZE = '.result-all > span';
    private const ID = '.items-list--tem-id';
    private const DATE = '.items-list--item--date';
    private const TITLE = '.col-md-8 > a > span';
    private const TYPE = '.breadcrumb > li ';
    private const COMPANY = '.items-list-item-description';
    private const PRICE = '.items-list--item--price';
    private const DESCRIPTION = '.description';

    private $tendersIds;//all tenders in our db

    private $currentRequest;
    private $requestTenders;//tenders for current request

    private $tendersToAdd;//array contain all new tenders
    private $tendersToUserUpdate; // array contain changed tenders

    private $messages;

    /**
     * Function receive Request model
     * Than load from database All tenders ids  (Used when we already have tender to dodge duplications)
     *
     *
     * Than function calls method Update or ignore
     * Return array of strings
     * Strings contain messages of completed work
     * For example (INSPECTED ID... or ADDED ID...)
     *
     * @param GlobalRequest $model
     */
    public function parse($model)
    {
        /** @var TendersRepository $tenderRepository */
        $tenderRepository = app(TendersRepository::class);

        $this->currentRequest = $model;
        $this->tendersIds = array_flip($tenderRepository->getAllIds()->pluck('tender')->toArray());//Get all tenders IDs from DB
        $this->prepareMessageArray(); //Init arrays of messages to return them as result

        $this->updateOrIgnore();

        $this->messages['all'] = $this->messages['Added'] . $this->messages['Already have'] . $this->messages['Unsubscribed'];

        return $this->messages;
    }

    /**
     * @param $request
     * @return bool
     */
    public function CheckRequest($request)
    {
        $client = new Client(self::GUZZEL_PROPERTIES);
        $crawler = new Crawler();
        try {
            $body = $client->get('search/?' . $request)
                ->getBody();
            $crawler->addContent($body);
            $crawler->filter(self::SIZE)->text();// short pass for size value
        } catch (RequestException $exception) {
            $exception->getResponse()->getBody(true);
            return false;
        }
        return true;
    }

    /**
     * Function call Method to inspect if we have changes in size
     * If size is same we ignore this request until next time
     * else function loads request tenders ( Will be used to inspect tenders changes)
     * And calls methods:
     * Search Updates Loads pages with items and inspecting them if we have it already
     * Add them if we hevent them
     * Add method inserts array of new tenders to db
     * Updated delete relations between request tender
     * @return void
     */
    private function updateOrIgnore()
    {
        $client = new Client(self::GUZZEL_PROPERTIES);


        $size = $this->getSize($client);//Call function to check Count of tenders in Prozorro
        if ($this->currentRequest->last_updated_size != $size && $size !== null) {
            $this->messages['targets'] .= "This request " . $this->currentRequest->request . " have some changes" . PHP_EOL;
            $this->searchForUpdates($client, $size);
            $this->add();//Add all new tenders to DB
            $this->update();//Delete all Changed Tenders from global_request_tender

            /** @var GlobalRequestRepository $globalRequestRepository */


            $globalRequestRepository = app(GlobalRequestRepository::class);
            $globalRequestRepository->update($this->currentRequest, $size);//Update count of items
        } else {
            $this->messages['targets'] .= "This request : " . $this->currentRequest->request . "  Up to date" . PHP_EOL;
        }
    }

    /**
     * Function init array fields
     * @return void
     */
    private function prepareMessageArray()
    {
        $this->messages['targets'] = '';
        $this->messages['Added'] = '';
        $this->messages['Already have'] = '';
        $this->messages['Unsubscribed'] = '';
        $this->messages['errors'] = '';
    }

    /**
     * Function makes request to target Site (Prozorro)
     * Then crawler inspect page for count of items (Tenders )
     * if every thing fine we return size
     * other way we  throw Exception
     * @param $client
     * @return int|void
     * @throws Exception
     */
    private function getSize($client)
    {
        $crawler = new Crawler();
        try {
            $body = $client->get('search/?' . $this->currentRequest->request)
                ->getBody();
            $crawler->addContent($body);
            $node = $crawler->filter(self::SIZE);// short pass for size value

            $size = $node->count() ? (int)str_replace(' ', '', $node->text()) : 0;
            $crawler->clear();
            return $size;
        } catch (RequestException $exception) {
            $this->messages['errors'] .= 'Something went wrong with this Request :' . PHP_EOL . Psr7\str($exception->getRequest());
        }

    }

    /**
     * Function makes POST request to site and takes response
     * if its fine we send it to crawler method
     * This function call Post request for Size/10  times
     *
     * @param Client $client
     * @param int $size
     *
     * @return void
     */
    private function searchForUpdates($client, $size)
    {
        $this->requestTenders = $this->currentRequest->tenders()->select('tender', 'status')->get()->pluck('status',
            'tender');//Get our Request Tenders Ids
        $i = 0;
        do {
            try {
                $body = $client->post('search/?' . $this->currentRequest->request . 'start=' . $i)
                    ->getBody();
            } catch (RequestException $exception) {
                $this->messages['errors'] .= 'Something went wrong with this Request :' . PHP_EOL . Psr7\str($exception->getRequest());
            }
            if (!empty($body)) {
                $this->getItems($body);
                $i += 10;
            } else {
                return;
            }
        } while ($i < $size);
    }


    /**
     * Function receive dom page
     * And send it to crawler with path's
     * After crawler find searched elements function sends it checkItem method
     * @param Psr7\Stream $body
     */
    private function getItems($body)
    {

        $crawler = new Crawler();
        $crawler->addContent($body); // attach out html code to crawler

        $recommends = $crawler->filter('.items-list');
        $recommends->count() ? $recommends->each(function (Crawler $node) {

            $recommends = $node->filter(self::DESCRIPTION);//In Prozorro some times they have empty description
            //To dodge this problem we checked this value before inint out var

            $description = $recommends->count() ? $recommends->text() : 0;// Check value for empty if empty we just attache 0

            $recommends = $node->filter(self::TYPE)->last();//In Prozorro some times they have empty region
            $region = $recommends->count() ? $recommends->text() : 0;
            $recommends = $node->filter(self::DATE);//In Prozorro some times they have empty date
            $date = $recommends->count() ? $recommends->text() : 0;

            $this->checkItem(
                $node->filter(self::ID)->text(),//ID
                $node->filter(self::TITLE)->text(),//TITLE
                $description,//DESCRIPTION
                $node->filter(self::COMPANY)->text(),//COMPANY
                $node->filter(self::PRICE)->text(),//PRICE
                $node->filter(self::TYPE)->nextAll()->text(),//TYPE
                $region,//REGION
                $date//DATE
            );

        }) : 0;
    }

    /**
     *Function inspect tenders
     * if we found new Tender we add it to tendersToAdd
     * if we see tender witch we already have in our global_request_tender DB we delete it from tendersToUserUpdate
     * for all others tenders its seems that they changed
     * @param string $tenderId
     * @param string $title
     * @param string $description
     * @param string $company
     * @param string $value
     * @param string $status
     * @param string $region
     * @param string $date
     *
     * @return void
     */
    private function checkItem($tenderId, $title, $description, $company, $value, $status, $region, $date)
    {


        if (!empty($this->requestTenders[$tenderId])) { //Check if we already have this object for Current User Request
            if ($this->requestTenders[$tenderId] == $status) { //If we found this tender we delete record from Update Array
                $this->messages['Already have'] .= 'Tender  ' . $tenderId . ' already contained in DB' . PHP_EOL;
                $this->requestTenders->forget($tenderId);
            }
        } elseif (empty($this->tendersIds[$tenderId]))//Check if its record contains in our Database
        {
            $this->messages['Added'] .= $tenderId . PHP_EOL;
            $this->tendersToUserUpdate[] = [
                $title . '<br>' . 'https://prozorro.gov.ua/tender/' . substr($tenderId, '4') . '<br>'
            ];
            $this->tendersToAdd[] = [
                'tender' => $tenderId,
                'title' => $title,
                'description' => $description,
                'company' => $company,
                'value' => $value,
                'status' => $status,
                'region' => $region,
                'date' => $date
            ];
        } else {
            /** @var TendersRepository $tenderRepository */
            $tenderRepository = app(TendersRepository::class);
            $id = $tenderRepository->searchBySiteId($tenderId)->id;
            /** @var GlobalRequestTenderRepository $globalRequestTenderRepository */
            $globalRequestTenderRepository = app(GlobalRequestTenderRepository::class);
            $globalRequestTenderRepository->add($this->currentRequest->id, $id);
            $this->tendersToAdd[] = [
                'tender' => $tenderId,
                'title' => $title,
                'description' => $description,
                'company' => $company,
                'value' => $value,
                'status' => $status,
                'region' => $region,
                'date' => $date
            ];
        }
    }


    /**
     * Function prepare array to send it to user
     * @return void
     */
    private function update()
    {

        if (!$this->requestTenders->isEmpty() || !empty($this->tendersToAdd)) {

            $tenders = null;
            $this->requestTenders = array_flip($this->requestTenders->toArray());


            if (!empty($this->requestTenders)) {
                foreach ($this->requestTenders as $requestTender) {
                    $tenders[] = [
                        'tender' => $requestTender,
                        'link' => 'https://prozorro.gov.ua/tender/' . substr($requestTender, 4),
                    ];
                }
            }
            if (!empty($this->tendersToAdd)) {
                foreach ($this->tendersToAdd as $requestTender) {
                    $tenders[] = [
                        'tender' => $requestTender['tender'],
                        'link' => 'https://prozorro.gov.ua/tender/' . substr($requestTender['tender'], 4),
                    ];
                }
            }

            foreach ($this->requestTenders as $node) {
                $this->messages['Unsubscribed'] .= $node . PHP_EOL;
                /** @var TendersRepository $tendersRepository */
                $tendersRepository = app(TendersRepository::class);
                $tenderModel = $tendersRepository->searchBySiteId($node);
                $globalRequestTenderRepository = app(GlobalRequestTenderRepository::class);
                $globalRequestTenderRepository->delete($this->currentRequest->id, $tenderModel->id);
            }
              $this->sendMail($tenders);
        }
    }

    private function sendMail($tenders)
    {
        if (!empty($tenders)) {

            $email = User::find($this->currentRequest->owner_id)->email;

            Notification::route('mail', $email)
                ->route('nexmo', '5555555555')
                ->route('slack', 'https://hooks.slack.com/services/...')
                ->notify(new TendersChanged($tenders));
        }
    }

    /**
     * Function insert all new tenders to db
     *
     * @return  void
     */
    private function add()
    {

        if (!empty($this->tendersToAdd)) {
            $records = count($this->tendersToAdd);
            /** @var TendersRepository $tendersRepository */
            $tendersRepository = app(TendersRepository::class);
            $lastRecordId = $tendersRepository->getLastId();
            if (empty($lastRecordId)) {
                $lastRecordId = 1;
            } else {
                $lastRecordId = $lastRecordId->id + 1;
            }

            $tendersRepository->insert($this->tendersToAdd);
            $requestTenderAdd = null;
            for ($i = 0; $i < $records; $i++) {
                $requestTenderAdd[] = [
                    'tender_id' => $lastRecordId,
                    'global_request_id' => $this->currentRequest->id
                ];
                $lastRecordId++;
            }
            /** @var GlobalRequestTenderRepository $globalRequestTenderRepository */
            $globalRequestTenderRepository = app(GlobalRequestTenderRepository::class);
            $globalRequestTenderRepository->insert($requestTenderAdd);
            $this->messages['targets'] .= "All new tenders added successfully" . PHP_EOL;
        }
    }
}
