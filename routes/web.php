<?php

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Route::get('/send','NotificationController@send')->name('send');


Auth::routes();


Route::middleware('auth')->group(function(){

    Route::get('/', 'AjaxController@index')->name('home');
    Route::get('/user/requests/','AjaxController@requests')->name('users.requests');
    Route::get('/user/request/tenders','AjaxController@tenders')->name('user.request.tenders');
    Route::get('/user/request/tender','AjaxController@tender')->name('user.request.tender');

    Route::get('/user/request/form/','AjaxController@requestFormGet')->name('user.request.form');
    Route::post('/user/request/form/post','AjaxController@postRequest')->name('user.request.form.get');
    Route::get('/user/request/form/type/parameters/','AjaxController@parameters')->name('user.request.form.type.parameters');
});


