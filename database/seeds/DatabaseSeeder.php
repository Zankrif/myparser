<?php

use App\Http\Repositories\StatusSettingsRepository;
use App\Http\Repositories\RegionSettingsRepository;
use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    CONST STATUS_VARIABLES_REQUEST = [
        'active.enquiries',
        'active.tendering',
        'active.auction',
        'active.qualification',
        'active.qualification.stand-still',
        'active.awarded',
        'unsuccessful',
        'cancelled',
        'complete',
        'active.pre-qualification',
        'active.pre-qualification.stand-still',
        'draft.pending',
        'draft.unsuccessful'];
    CONST STATUS_VARIABLES_USER =  [
        'Период уточнения',
        'Подача предложенй',
        'Аукцион',
        'Квалификация победителя',
        'Квалификация победителя (Период оспаривания)',
        'Предложения расмотрены',
        'Торги не состоялись',
        'Торги отменены',
        'Завершен',
        'Переквалификация',
        'Переквалификация (Период оспраивания)',
        'Неактивное приглашение',
        'Неуспешная закупка',
    ];
    CONST REGION_VARIABLES_REQUEST = [
        '91-94',
        '36-39',
        '10-13',
        '43-45',
        '79-82',
        '58-60',
        '83-87',
        '18-20',
        '61-64',
        '40-42',
        '95-98',
        '25-28',
        '88-90',
        '29-32',
        '99',
        '07-09',
        '65-68',
        '14-17',
        '46-48',
        '76-78',
        '73-75',
        '21-24',
        '54-57',
        '33-35',
        '49-53',
        '01-06',
        '69-72',
    ];
    CONST REGION_VARIABLES_USER = [
        'Луганская область',
        'Полтавская область',
        'Житомерская область',
        'Волынская область',
        'Львовская область',
        'Черновицкая область',
        'Донецкая область',
        'Черкаская область',
        'Харьковская область',
        'Сумская область',
        'Автономная Республика Крым',
        'Кировоградская область',
        'Закарпатская область',
        'Хмельницкая область',
        'город Севастополь',
        'Киевская область',
        'Одесская область',
        'Черниговская область',
        'Тернопольская область',
        'ИваноФранковская область',
        'Херсонская область',
        'Винницкая область',
        'Николаевская область',
        'Ровенская область',
        'Днепропетровская область',
        'город Киев',
        'Запоржская область',
    ];
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {

        // $this->call(UserSeeder::class);
        \App\RequestType::insert([['start_of_request'=>'status=','name'=>'Статус'],['start_of_request'=>'region=','name'=>'Регион']]);


        for ($i = 0; $i < count(self::STATUS_VARIABLES_REQUEST); $i++) {
            app(\App\Http\Repositories\SettingsRepository::class)->add(
                1,
                self::STATUS_VARIABLES_REQUEST[$i],
                self::STATUS_VARIABLES_USER[$i]
            );
        }
        for ($i = 0; $i < count(self::REGION_VARIABLES_REQUEST); $i++) {
            app(\App\Http\Repositories\SettingsRepository::class)->add(
                2,
                self::REGION_VARIABLES_REQUEST[$i],
                self::REGION_VARIABLES_USER[$i]
            );
        }

    }
}
