<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class GlobalRequestSettings extends Model
{
    protected $fillable = [
        'global_request_id',
        'settings_id',
    ];
}
