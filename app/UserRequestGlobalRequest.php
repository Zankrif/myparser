<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class UserRequestGlobalRequest extends Model
{
    protected $fillable = [
        'user_request_id',
        'global_request_id',
    ];
}
