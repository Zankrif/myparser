<?php


namespace App\Http\Repositories;


use App\Settings;
use App\Tenders;

class TendersRepository
{
    /**
     * @param $tenderId
     * @return Tenders|null
     */
    public function searchBySiteId($tenderId): ?Tenders
    {
        return Tenders::where('tender',$tenderId)->first();
    }

    /**
     * @param string $tenderId
     * @param string $title
     * @param string $description
     * @param string $company
     * @param string $value
     * @param string $status
     * @param string $region
     * @param string $date
     * @return Tenders
     */
    public function create($tenderId, $title, $description, $company, $value, $status, $region, $date)
    {
       return Tenders::create([
            'tender' => $tenderId,
            'title' => $title,
            'description' => $description,
            'company' => $company,
            'value' => $value,
            'status' => $status,
            'region' => $region,
            'date'=>$date,
        ]);
    }

    /**
     * @param Tenders $model
     * @param string $status
     * @return bool
     */
    public function update($model, $status)
    {
        return $model->update(['status'=>$status]);
    }

    /**
     * @return Tenders[]|null
     */
    public function getAllIds()
    {
        return Tenders::select('tender')->get();
    }

    /**
     * @return Tenders|null
     */
    public function getLastId()
    {
        return Tenders::latest()->first();
    }

    /**
     * @param array $array
     * @return void
     */
    public function insert($array)
    {
        Tenders::insert($array);
    }
}
