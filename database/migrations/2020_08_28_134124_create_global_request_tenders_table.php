<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateGlobalRequestTendersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('global_request_tenders', function (Blueprint $table) {
            $table->id()->unique();
            $table->timestamps();
        });
        Schema::table('global_request_tenders',function (Blueprint $table){
            $table->unsignedBigInteger('global_request_id');
            $table->foreign('global_request_id')->references('id')->on('global_requests');
            $table->unsignedBigInteger('tender_id');
            $table->foreign('tender_id')->references('id')->on('tenders');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('global_request_tenders');
    }
}
