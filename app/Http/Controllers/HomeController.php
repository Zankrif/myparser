<?php

namespace App\Http\Controllers;

use App\Http\Repositories\TargetsRepository;
use App\Http\Repositories\UserRequestRepository;
use App\UserRequest;
use Illuminate\Http\Request;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     * @param Request $request
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index(Request $request)
    {
        $paginate =  UserRequest::where('user_id',$request->user()->id)->paginate(2);
        $links = $paginate->links();
        $requests =  $paginate->each(function ($node){
            return $node['requests'] = $node->request()->first();
        });

        return view('home',compact('requests','links'));
    }
}

