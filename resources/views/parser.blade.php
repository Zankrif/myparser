@extends('layouts.app')
@section('scripts')
    <script src="{{ asset('js/parser.js') }}" defer></script>
@endsection
@section('content')
    <div class="content " style="display: flex">
        <div class="menu bg-white shadow-sm br-smooth align-item justify-content-center form-inline  d-block text-lg-center " style="font-size: 15px" >
            <nav class="navbar navbar-light">
               <ul class="navbar-nav ">
                    <li class="nav-item">
                        <a  class="nav-link" onclick="getRequests()" >Просмотреть запросы</a>
                        <a class="nav-link" onclick="getRequestForm()">Создать запрос</a>
                    </li>
               </ul>
            </nav>
        </div>
        <div id="user-space" class="user-content bg-white shadow-sm justify-content-center d-block br-smooth" style="display: none" >

        </div>
    </div>

@endsection
