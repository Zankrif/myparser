<?php


namespace App\Http\Services;

use App\GlobalRequest;
use App\Http\Repositories\GlobalRequestSettingsRepository;
use App\Http\Repositories\GlobalRequestRepository;
use App\Http\Repositories\RequestTypeRepository;
use App\Http\Repositories\UserRequestGlobalRequestRepository;
use App\Http\Repositories\UserRequestRepository;
use App\RequestType;

class RequestService
{
    const MINVALUE = 0;//Index for array of Values
    const MAXVALUE = 1;//Index for array of Values

    const TAG_ID = 0;//Index for array of settings
    const TAG_START_POINT = 1;//Index for array of settings
    const TAG_REQUEST_VALUE = 2;//Index for array of settings

    private $requestId;//Primary key of Global Request table
    private $userRequestId;//Primary key of User Request table
    private $requestTags;//Contain Ids of Settings table for relations with Global Request

    /**
     * Function returns all Types as Collection of models
     *
     * @return RequestType[]|\Illuminate\Database\Eloquent\Collection
     */
    public function getRequestsTypes()
    {
        /** @var RequestTypeRepository $requestTypeRepository */
        $requestTypeRepository = app(RequestTypeRepository::class);
        return $requestTypeRepository->getAll();
    }

    /**
     * Function return all Settings for This Type
     *
     * @param int $typeId
     * @return \Illuminate\Database\Eloquent\Collection|null
     */
    public function getParameters($typeId)
    {
        /** @var RequestTypeRepository $requestTypeRepository */
        $requestTypeRepository = app(RequestTypeRepository::class);
        return $requestTypeRepository->parameters($typeId);
    }

    /**
     * Function receive values from User filled Form
     * Sending them to formatting functions
     * After formatting sending request values to database
     *
     * Used Database tables are:
     * User Request - represent user request and contain user filled name
     *
     * Global Request - it is a folder for prepared for parse Request
     * Used once for ore all user with same Requests
     *
     * User_Request_Global_Request table contain relations.
     * between User Request an Global Request
     *
     * @param int $userId Creator (User) id in Users table
     * @param string $name User filed string, helps him identificate his Request
     * @param array $value Array of values 'integers' can be null.
     * @param array $settings Array contain rows of parameters from Form
     * @return void
     */
    public function add($userId, $name, $value, $settings)
    {

        $this->convertInputValue($settings, $value);// This function change original Setting and values to strings
        /** UserRequestRepository $userRequestRepository */
        $userRequestRepository = app(UserRequestRepository::class);
        $this->userRequestId = $userRequestRepository->add($userId, $name)->id;
        $this->firstOrCreate($value, $settings);
        $this->addRelations();
    }

    /**
     * Function receive a id of Request and  call Repository method
     * return a model of Global Request or null if request dosen't exist any more
     *
     * @param int $requestId
     * @return GlobalRequest|null
     */
    public function getRequestById($requestId)
    {
        /** @var GlobalRequestRepository $globalRequestRepository */
        $globalRequestRepository = app(GlobalRequestRepository::class);
        return $globalRequestRepository->find($requestId);
    }


    /**
     * Function receive arrays from User Request form
     * and send them to other functions
     * to make usable variables
     *
     * @param array $settings
     * @param array $value
     * @return void
     */
    private function convertInputValue(array &$settings, &$value)
    {
        $this->requestTags = $this->prepareRequestTags($settings);
        if (!empty($value)) {
            $value = $this->prepareValueString($value);
        }
        $settings = $this->prepareRequestString($value, $this->requestTags);
    }


    /**
     * Function receive a formed strings of value and request
     * Than function will find Same Request or create new one
     * After this function create new relation between User Request and Global Request
     * @param string $value
     * @param string $request
     * @return void
     */
    private function firstOrCreate($value, string $request)
    {
        /** @var GlobalRequestRepository $globalRequestRepository */
        $globalRequestRepository = app(GlobalRequestRepository::class);
        $requestModel = $globalRequestRepository->firsOrCreate($request, $value);
        $this->requestId = $requestModel;
        $this->addRelationsToGlobalRequest(); // Add relations between Global Request and Settings
    }

    /**
     * Function takes private variables ( userRequestId and GlobalRequestId)
     * of new User Request and form relations between Tables by
     * making new record in to User_Request_Global_Requests table
     *
     * @return void
     */
    private function addRelations()
    {
        /** @var UserRequestGlobalRequestRepository $userRequestGlobalRequestRepository */
        $userRequestGlobalRequestRepository = app(UserRequestGlobalRequestRepository::class);
        $userRequestGlobalRequestRepository->create($this->userRequestId, $this->requestId);
    }

    /**
     * Function takes private array of settings values of new Global Request
     * Then function prepares array to inserting it to Global_Request_Setting table
     * Its helps us in situations when Parameters ( Settings ) of request could be Changed in Target Site
     * To update all Global Requests witch contain this parameter
     *
     * @return void
     */
    private function addRelationsToGlobalRequest()
    {
        /** @var GlobalRequestSettingsRepository $globalRequestSettingsRepository */

        $globalRequestSettingsRepository = app(GlobalRequestSettingsRepository::class);
        if (empty($globalRequestSettingsRepository->first($this->requestId))) {

            $insertRelations = null;
            foreach ($this->requestTags as $node) {

                $insertRelations[] = ['settings_id' => $node['tag_id'], 'global_request_id' => $this->requestId];
            }
            $globalRequestSettingsRepository->insert($insertRelations);
        }
    }

    /**
     * Function convert Array from Form to array with variables
     * Array witch arrived from Form contain a String value with ( , ) as separator
     * Function read each node of arrived array
     * and explode string
     * After all  Function returns  new array for further use
     *
     * @param array $settings
     * @return array
     */
    private function prepareRequestTags($settings)
    {
        $array = null;
        foreach ($settings as $node) {
            $node = explode(',', $node);
            $array[] = [
                'tag_id' => $node[self::TAG_ID],
                'startPoint' => $node[self::TAG_START_POINT],
                'request_value' => $node[self::TAG_REQUEST_VALUE]
            ];
        }
        return $array;
    }


    /**
     * Function takes array of Values from user filled Form.
     * Than function inspect values and form string
     * to be used in new Request as parameter (Setting)
     *
     * @param array $value
     * @return string|null
     */
    private function prepareValueString(array $value)
    {
        for ($i = 0; $i < count($value); $i++) {
            $value[$i] = preg_replace('~[^0-9]+~', '', $value[$i]);;
        }
        if (!empty($value[self::MINVALUE]) && !empty ($value[self::MAXVALUE])) {
            return $value[self::MINVALUE] . '-' . $value[self::MAXVALUE];
        } elseif (!empty($value[self::MINVALUE])) {
            return $value[self::MINVALUE];
        } elseif (!empty($value[self::MAXVALUE])) {
            return $value[self::MAXVALUE];
        }
        return null;
    }

    /**
     * Function form a request string. String will be used in POST && GET Requests without any changes
     * Formed string can be changed in situations when One of parameters changed in Target Site
     *
     * @param string $value its a formed string witch  contain an decimal value or 2 values with '-' between them
     * @param array $settings contain already converted values
     * @return string
     */
    private function prepareRequestString($value, array $settings)
    {
        $result = "";
        foreach ($settings as $tag) {
            $result .= $tag['startPoint'] . $tag['request_value'] . '&';
        }
        if (!empty($value)) {
            return $result . "value=" . $value . '&';
        }
        return $result;
    }
}
