<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateUserRequestGlobalRequestsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('user_request_global_requests', function (Blueprint $table) {
            $table->id()->unique();
            $table->timestamps();
        });
        Schema::table('user_request_global_requests',function (Blueprint $table){
            $table->unsignedBigInteger('user_request_id');
            $table->foreign('user_request_id')->references('id')->on('user_requests');
            $table->unsignedBigInteger('global_request_id');
            $table->foreign('global_request_id')->references('id')->on('global_requests');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('user_request_global_requests');
    }
}
