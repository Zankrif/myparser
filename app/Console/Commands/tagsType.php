<?php

namespace App\Console\Commands;

use App\Http\Services\SettingsService;
use App\RequestType;
use Illuminate\Console\Command;

class tagsType extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'tags:add {requestTypeId} {requestValue} {userValue}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     */
    public function handle()
    {
        $this->info(
            app(SettingsService::class)->addRequest(
                RequestType::find($this->argument('requestTypeId')),
                $this->argument('requestValue'),
                $this->argument('userValue')
            ),'v');
    }
}
